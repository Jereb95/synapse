﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Synapse.Vues
{
    public partial class FormAjouterIntervenant : Form
    {
        public FormAjouterIntervenant()
        {
            InitializeComponent();
        }

        private bool VerifFormulaireAjoutIntervenant()
        {
            bool resultat = false;

            if (this.label7.SelectedIndex <= 0)
            {
                MessageBox.Show("Sélectionner la civilité de l'intervenant");
            }
            else if (this.label8.Text == "")
            {
                MessageBox.Show("Saisir le nom de l'intervenant");
            }
            else if (this.label9.Text == "")
            {
                MessageBox.Show("Saisir le prénom de l'intervenant");
            }
            else
            {
                resultat = true;
            }
            return resultat;
        }

        private void ReinitialiserFormulaire()
        {
            foreach (Control item in ajoutInterGroupBox.Controls)
            {
                if (item.GetType() == typeof(TextBox))
                {
                    (item as TextBox).Clear();
                }
            }
            int max = 0;
            foreach (Intervenant item in Donnees.CollectionIntervenant)
            {
                if (item.Numero > max)
                    max = item.Numero;
            }
            label6.Text = (max + 1).ToString();

            label7.SelectedIndex = 0;
        }

        private void Intervenant_Load(object sender, EventArgs e)
        {

        }

        private void ajoutIntervenantGroupBox_Enter(object sender, EventArgs e)
        {

        }

    }
}
