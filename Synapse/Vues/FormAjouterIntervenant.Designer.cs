﻿namespace Synapse.Vues
{
    partial class FormAjouterIntervenant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ajoutInterGroupBox = new System.Windows.Forms.GroupBox();
            this.ButtonAjoutInter = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.ajoutInterGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ajoutInterGroupBox
            // 
            this.ajoutInterGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.ajoutInterGroupBox.Controls.Add(this.comboBox2);
            this.ajoutInterGroupBox.Controls.Add(this.textBox8);
            this.ajoutInterGroupBox.Controls.Add(this.textBox7);
            this.ajoutInterGroupBox.Controls.Add(this.textBox6);
            this.ajoutInterGroupBox.Controls.Add(this.textBox5);
            this.ajoutInterGroupBox.Controls.Add(this.label10);
            this.ajoutInterGroupBox.Controls.Add(this.label9);
            this.ajoutInterGroupBox.Controls.Add(this.label8);
            this.ajoutInterGroupBox.Controls.Add(this.label7);
            this.ajoutInterGroupBox.Controls.Add(this.label6);
            this.ajoutInterGroupBox.Controls.Add(this.ButtonAjoutInter);
            this.ajoutInterGroupBox.Location = new System.Drawing.Point(31, 12);
            this.ajoutInterGroupBox.Name = "ajoutInterGroupBox";
            this.ajoutInterGroupBox.Size = new System.Drawing.Size(542, 389);
            this.ajoutInterGroupBox.TabIndex = 0;
            this.ajoutInterGroupBox.TabStop = false;
            // 
            // ButtonAjoutInter
            // 
            this.ButtonAjoutInter.Location = new System.Drawing.Point(189, 327);
            this.ButtonAjoutInter.Name = "ButtonAjoutInter";
            this.ButtonAjoutInter.Size = new System.Drawing.Size(146, 41);
            this.ButtonAjoutInter.TabIndex = 0;
            this.ButtonAjoutInter.Text = "AJOUTER";
            this.ButtonAjoutInter.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(62, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Numero Intervenant";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(62, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Civilité";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(62, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Nom";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(62, 206);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Prénom";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(62, 260);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Taux Horaire";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(235, 44);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(170, 20);
            this.textBox5.TabIndex = 6;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(235, 147);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(170, 20);
            this.textBox6.TabIndex = 7;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(235, 203);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(170, 20);
            this.textBox7.TabIndex = 8;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(235, 257);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(170, 20);
            this.textBox8.TabIndex = 9;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(235, 91);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(170, 21);
            this.comboBox2.TabIndex = 10;
            // 
            // Intervenant
            // 
            this.ClientSize = new System.Drawing.Size(601, 431);
            this.Controls.Add(this.ajoutInterGroupBox);
            this.Name = "Intervenant";
            this.Text = "FORMULAIRE AJOUT INTERVENANT";
            this.ajoutInterGroupBox.ResumeLayout(false);
            this.ajoutInterGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox ajoutIntervenantGroupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button ButtonAjoutIntervenant;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox ajoutInterGroupBox;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ButtonAjoutInter;
    }
}