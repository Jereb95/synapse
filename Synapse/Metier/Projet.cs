﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    [Serializable]
    public class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixFactureMO;

        private List<Mission> _missions;


        public decimal MargeBruteCourante()
        {
            decimal resultat;
            resultat = this._prixFactureMO - this.CumulCountMO();
            return resultat;
            

        }
        private decimal CumulCountMO()
        {
            decimal resultat = 0;
            foreach (Mission missionCourante in _missions)
            {
                resultat += missionCourante.Executant.TauxHoraire * missionCourante.NbHeuresEffectuees();
            }

            return resultat;
        }

    }
}
