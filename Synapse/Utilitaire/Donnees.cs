﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Metier;

namespace Synapse.Utilitaire
{
    public static class Donnees
    {
        private static List<Projet> _collectionProjet;
        private static List<Intervenant> _collectionIntervenant;


        /// <summary>
        /// Obtient la liste des projets
        /// </summary>
        /// <remarks>Les activités contiennent les actions de formations et les sessions de formations</remarks>
        public static List<Projet> CollectionProjet
        {
            get
            {
                if (_collectionProjet== null)
                {
                    _collectionProjet = (List<Projet>)Persistances.ChargerDonnees("Projet");
                    if (_collectionProjet == null)
                        _collectionProjet = new List<Projet>();
                }
                return Donnees._collectionProjet;
            }
            set { Donnees._collectionProjet = value; }
        }
        /// <summary>
        /// Obtient la liste des intervenants
        /// </summary>
        public static List<Intervenant> CollectionIntervenant
        {
            get
            {
                if (_collectionIntervenant == null)
                {
                    _collectionIntervenant = (List<Intervenant>)Persistances.ChargerDonnees("Intervenant");
                    if (_collectionIntervenant == null)
                        _collectionIntervenant = new List<Intervenant>();
                }
                return Donnees._collectionIntervenant;
            }
        }


        /// <summary>
        /// Sérialise l'intégralité des données de l'application
        /// </summary>
        public static void SauvegardeDonnees()
        {
            Persistances.SauvegarderDonnees("Intervenant", _collectionIntervenant);
            Persistances.SauvegarderDonnees("Projet", _collectionProjet);

        }







    }
}
